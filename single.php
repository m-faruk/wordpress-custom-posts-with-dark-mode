<?php
/**
 * The template for displaying single posts and pages.
 * b) Create a basic single post template that contains any relevant meta data and the post content.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */
get_header();
?>

 <div class="row mt-5">
         <button id="dark-mode-toggle" class="dark-mode-toggle">
          <svg width="100%" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 496 496"><path fill="currentColor" d="M8,256C8,393,119,504,256,504S504,393,504,256,393,8,256,8,8,119,8,256ZM256,440V72a184,184,0,0,1,0,368Z" transform="translate(-8 -8)"/></svg>
        </button>
    </div>

<div class="container mt-5">
  <section class="blog-posts">   

    <div class="row">   
               <?php if ( have_posts() ) : 
                     	// Start the loop.
                        while ( have_posts() ) : the_post();
            		?>
                        <article class="single_post">
                           <div class="row">
                               <div class="col-md-6 col-sm-12 col-lg-6">
            		                    <div class="featured_image card-img-top">
                                  			<?php
                                  				if ( has_post_thumbnail() ) {
                                  					the_post_thumbnail( 'full', array( 'class'  => 'bck_img' ) );
                                  				}
                                  			?>
            		                  </div>                            
                           </div>

                           <div class="col-md-6 col-sm-12 col-lg-6">
                              <div class="title card-title">
                                    <h2 class="page-title text-primary"> <?php the_title(); ?></h2>
                                    <hr/>
                                     <p class="card-text"> <?php the_author(); ?> vom <?php the_date("j. F Y");  ?></p>                          <hr/>
                               </div>
                           </div>
                         </div>

                             <div class="row">
                               <div class="col-md-6 col-sm-12 col-lg-6">
                                <p class="card-text"> <?php the_content(); ?></p>
                              </div>
                            </div>
                    
                          <div class="row text-center">
                               <div class="col-md-4 col-sm-12 col-lg-4">
                               <a class="btn btn-outline-secondary" href="http://localhost/itdesign/" title="Zu allen Beiträgen" aria-label="Zu allen Beiträgen">Zu allen Beiträgen</a>
                              </div>
                            </div>

                             </article><!--end of single_post-->
                         <?php
                       // End the loop.
                       endwhile;
                        endif;
                        itdesign_pagination();
                        ?>
             </div>
        
    </section><!--end of blog_posts-->
	
</div><!--end of container-->
<?php get_footer(); ?>
