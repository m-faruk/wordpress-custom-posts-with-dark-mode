# WordPress Programming Challenge

![datei](screenshot.png)

## Introduction
This repository contains a lightweight WordPress theme with Bootstrap 4.5.0. In this programming task you need to implement a shortcode that can be used to display the last posts in a responsive grid. Furthermore you have to create a small template to display a single post. In a second step a "Dark Mode" shall be implemented to switch the color scheme depending on the current time of the day.  

Please try to avoid using WordPress plugins. If you use any anyway, please don't forget to add a database dump to the repository you're working in.

## Preparation
1. Set up a local development environment (f.e. LAMP-/LEMP-Stack) and install WordPress
2. Clone this theme repository to your WordPress environment and activate it in your WordPress system
4. Install the WordPress plugins **Classic Editor** and **FakerPress**
5. Generate some random posts: Write them on your own or use your recently installed plugin FakerPress (Notice: Only use "Lorem Picsum" as image provider to avoid placeholder images)

## Task Details
a) Implement a shortcode that retrieves the latest 4 posts as cards as seen on the [itdesign CRM site](https://crm.itdesign.de/aktuelles/ "Card Example"). Parts of those cards should be:  
- Featured Image
- Title
- Short Excerpt
- Post Date in the format '01. January 2020'.

There should only be one post per row on smaller devices and as it gets larger multiple posts can be positioned next to each other. Sort these posts ascending by their title. Add links to the cards so if a user clicks on it he gets directed to the related single post. Use this shortcode on any page to test its function.

b) Create a basic single post template that contains any relevant meta data and the post content.

c) Add a button to all of your pages which can be used to enable a "Dark Mode". Whenever a user clicks on that button the color scheme changes to improve the visibility at low light. The Dark Mode should be enabled by default between 8pm and 6am. The button to switch the color mode can be used anytime. If a user switches to another site the selected (current) color scheme should stay the same. 

## Hints
[WordPress Developer Resources](https://developer.wordpress.org/)  
[WordPress Template Hierarchy](https://developer.wordpress.org/themes/basics/template-hierarchy/)  
[Bootstrap Documentation](https://getbootstrap.com/docs/4.5/getting-started/introduction/)  
[jQuery API Documentation](https://api.jquery.com/)
