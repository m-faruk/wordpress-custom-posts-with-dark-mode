<?php 

/** * index.php * * @link https://developer.wordpress.org/files/2014/10/Screenshot-2019-01-23-00.20.04.png */ 

get_header(); 

?>

<div id="primary">

    <div class="row mt-5">
             <button id="dark-mode-toggle" class="dark-mode-toggle">
              <svg width="100%" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 496 496"><path fill="currentColor" d="M8,256C8,393,119,504,256,504S504,393,504,256,393,8,256,8,8,119,8,256ZM256,440V72a184,184,0,0,1,0,368Z" transform="translate(-8 -8)"/></svg>
            </button>
    </div>

    <main id="main" class="site-main mt-5" role="main">        
  
        <?php 
            $args=array( 
                'post_type'=> 'post',
                'posts_per_page' => 4,
                'publish_status' => 'published',
                 ); 
            $newsposts = new WP_Query( $args ); if ( have_posts() ) : ?>

        <div class="container">
            <?php if ( is_home() && ! is_front_page() ){

             ?>

            <header class="mb-5">
                <h1 class="page-title screen-reader-text">
                    <?php single_post_title(); ?>
                </h1>
            </header>

            <?php } ?>

            <div class="row">
                <?php $index=0 ; $no_of_columns=2 ; while ( $newsposts->have_posts() ) : 
                $newsposts->the_post(); if ( 0 === $index % $no_of_columns ) { 
                ?>

                <div class="col-lg-4 col-md-6 col-sm-12">
                <?php } ?>

                    <article class="single-post">
                        <div class="featured_image card-img-top">
                            <a href="<?php the_permalink(); ?>">
                                <?php the_post_thumbnail( 'full', array( 'class'=> 'bck_img' )); ?></a>
                        </div>

                        <p class="card-text">
                            <?php the_author(); ?> vom
                            <?php the_date( "j. F Y"); ?>
                        </p>
                        <hr/>

                        <div class="title card-title">
                            <h3 class="page-title blog screen-reader-text"><a href="<?php the_permalink(); ?>"> <?php the_title(); ?></a></h3>
                        </div>

                        <p>
                            <?php $my_content=apply_filters( 'the_content', get_the_content() ); echo wp_trim_words( $my_content, 20); ?>
                            <a href="<?php the_permalink(); ?>"> mehr lesen </a>
                        </p>

                    </article>
                    <!--end of single_post-->

                    <?php $index ++; if ( 0 !==$index && 0===$index % $no_of_columns ) { ?>
                </div>
                <?php } endwhile; ?>
            </div>
        </div>
        <?php else : echo "Sorry, there are no posts found! Please create a post, you can see this post here!"; endif; ?>
        <div class="container">
            <div class="row">
                <?php itdesign_pagination(); ?>
            </div>
        </div>
    </main>
</div>

<?php get_footer();