<?php
/**
 * functions.php
 */

add_action( 'wp_enqueue_scripts', function () {
    /**
     * Styles
     */
    wp_enqueue_style( 'bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css' );
    wp_enqueue_style( 'roboto', get_template_directory_uri() . '/assets/css/font/roboto.css' );
    wp_enqueue_style( 'fontawesome', get_template_directory_uri() . '/assets/css/font/fontawesome.min.css' );
    wp_enqueue_style( 'main', get_template_directory_uri() . '/style.css', array( 'bootstrap', 'roboto', 'fontawesome' ) );

    /**
     * Scripts
     */
    wp_deregister_script( 'jquery' );
    wp_enqueue_script( 'jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js', [], '3.5.1', true );
    wp_enqueue_script( 'popper', 'https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js', [], '1.16.0', true );
    wp_enqueue_script( 'bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js', array( 'jquery', 'popper' ), '4.5.0', true );
    wp_enqueue_script( 'myscript', get_template_directory_uri() . '/myscript.js', array ( 'jquery' ), 1.1, true);

} );

add_action( 'after_setup_theme', function () {
    add_theme_support( 'html5' );
    add_theme_support( 'post-thumbnails' );
    add_image_size( 'featured-large', 350, 233, true);
    add_theme_support( 'title-tag' );
} );


/********************************************************************************************************

 a) Implement a shortcode that retrieves the latest 4 posts as cards as seen on the itdesign CRM site.
  Parts of those cards should be:

-Featured Image
-Title
-Short Excerpt
-Post Date in the format '01. January 2020'.

There should only be one post per row on smaller devices and as it gets larger multiple posts can be positioned next to each other. Sort these posts ascending by their title. Add links to the cards so if a user clicks on it he gets directed to the related single post. Use this shortcode on any page to test its function.

 **********************************************************************************************************/


// >> Create Shortcode to Display [itdesign-post] Post Types
 
function itdesign_shortcode__post_type(){
 
    $args = array(
                    'post_type'      => 'post',
                    'posts_per_page' => '4',
                    'publish_status' => 'published',
                 );
 
    $query = new WP_Query($args);
 
    if($query->have_posts()) :
 
        while($query->have_posts()) :
 
            $query->the_post() ;
                     
        $result .= '<div class="card>';
        $result .= '<div class="itdesign-poster">' . get_the_post_thumbnail() . '</div>';
        $result .= '<div class="itdesign-name">' . the_author() . 'vom' . the_date("j. F Y") . '</div>';
        $result .= '<div class="itdesign-title">' . get_the_title() . '</div>';

        $result .= '<div class="itdesign-desc">' . get_the_content() . '</div>'; 
        $result .= '</div>';
 
        endwhile;
 
        wp_reset_postdata();
 
    endif;    
 
    return $result;            
}
 
add_shortcode( 'itdesign-post', 'itdesign_shortcode__post_type' ); 

// shortcode code ends here

 
//Sort these posts ascending by their title.

function post_default_order( $query ){
    if( 'itdesign-post' == $query->get('post_type') ){
        if( $query->get('orderby') == '' )
            $query->set('orderby','title');

        if( $query->get('order') == '' )
            $query->set('order','ASC');
    }
}

add_action('pre_get_posts','post_default_order');




//pagination

function itdesign_pagination() {


$allowed_tags = [
        'span' => [
            'class' => []
        ],
        'a' => [
            'class' => [],
            'href' => [],
        ]
    ];

    $args = [
        'before_page_number' => '<span class="btn border border-secondary mr-2 mb-2">',
        'after_page_number' => '</span>',
    ];

    printf( '<nav class="itdesign-pagination clearfix">%s</nav>', wp_kses( paginate_links( $args ), $allowed_tags ) );

    }